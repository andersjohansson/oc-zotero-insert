;;; oc-zotero-insert.el --- Insert org cites from zotero          -*- lexical-binding: t; -*-

;; Copyright (C) 2021  Anders Johansson

;; Author: Anders Johansson <mejlaandersj@gmail.com>
;; Keywords: wp, convenience, bib
;; Created: 2021-05-07
;; Modified: 2021-07-05
;; Version: 0.1

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;; Insert org citations via zotero, using better bibtex CAYW
;; functionality. See:
;; https://retorque.re/zotero-better-bibtex/citing/cayw/

;;; Code:

(require 'org)
(require 'oc)
(require 'subr-x)
(require 'org-element)


;; FIXME: no support for editing and stuff that is needed
;; (org-cite-register-processor 'zotero
;;   :insert #'org-cite-zotero-insert)

(defvar oc-zotero-short-locator-labels
  '(("article" . "art.")
    ("chapter" . "ch.")
    ("subchapter" . "subch.")
    ("column" . "col.")
    ("figure" . "fig.")
    ("line" . "l.")
    ("note" . "n.")
    ("issue" . "no.")
    ("opus" . "op.")
    ("page" . "p.")
    ("paragraph" . "para.")
    ("subparagraph" . "subpara.")
    ("part" . "pt.")
    ("rule" . "r.")
    ("section" . "sec.")
    ("subsection" . "subsec.")
    ("Section" . "Sec.")
    ("sub verbo" . "sv.")
    ("schedule" . "sch.")
    ("title" . "tit.")
    ("verse" . "vrs.")
    ("volume" . "vol."))
  "Translations of locator labels.")

;;;###autoload
(defun org-cite-zotero-insert ()
  "Insert org citations via zotero.
Using better bibtex CAYW functionality. See:
https://retorque.re/zotero-better-bibtex/citing/cayw/"
  (interactive)
  (let (json suppress-author contents)
    (if-let ((buf (url-retrieve-synchronously "http://127.0.0.1:23119/better-bibtex/cayw?format=json")))
        (progn
          (with-current-buffer buf
            (when (re-search-forward "^$" nil t)
              (setq json (json-parse-buffer
                          :object-type 'alist
                          :array-type 'list
                          :false-object nil))))
          (setq contents
                (cl-loop for cit in json
                         concat
                         (let ((suffix ""))
                           (let-alist cit
                             (when (and (not suppress-author) .suppressAuthor)
                               (setq suppress-author t))
                             (setq suffix (unless (string-blank-p .suffix)
                                            (concat " " .suffix)))
                             (when (not (string-blank-p .locator))
                               (setq
                                suffix
                                (concat
                                 " "
                                 (alist-get .label oc-zotero-short-locator-labels .label nil #'equal)
                                 " "
                                 .locator
                                 suffix)))
                             (org-element-citation-reference-interpreter
                              (list 'citation-reference
                                    (list :key .citekey
                                          :prefix (unless (string-blank-p .prefix)
                                                    (concat .prefix " "))
                                          :suffix suffix))
                              nil)))))
          (insert (org-element-citation-interpreter
                   (list 'citation
                         (list :style (when suppress-author "year")))
                   contents))

          )
      (message "Error"))

    ))



(provide 'oc-zotero-insert)
;;; oc-zotero-insert.el ends here
